<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'openSUSE-id',
            'email' => 'admin@opensuse.id',
            'password' => bcrypt('openSUSE19'),
        ]);
    }
}
