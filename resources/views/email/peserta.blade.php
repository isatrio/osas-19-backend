<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tiket GNOME.Asia Summit 2019</title>
</head>
<style>
	tr,td{
		border-width:1px;
		border-style:solid;
	}
</style>
<body>
    <p>Dear <b>{{ $dataEmail['name'] }}</b>, </p>
    <br/>
    <p>
        Terima kasih telah mendaftar pada acara openSUSE.Asia Summit 2019.
    </p>
    <p>Berikut informasi untuk Anda.</p>
    <p>
        Tautan QRCode untuk pembayaran tiket GNOME.Asia Summit 2019 Anda dapat diakses <a href="{{ $dataEmail['message'] }}" target="_blank" rel="noopener noreferrer">di sini.</a>
    </p>
    <p>Silakan pindai QRCode tersebut dengan MyCOOP untuk melakukan pembayaran.</p>
    <p>QRCode untuk pembayaran tiket GNOME.Asia Summit 2019 Anda berlaku hanya 24 jam sejak Anda menerima email ini.</p>
    <p>Untuk cara pembayaran, silakan melihat panduannya <a href="https://2019.gnome.asia/#schedule" target="_blank" rel="noopener noreferrer">di sini.</a></p>
    <p>Jika Anda belum membayar, maka Anda belum terdaftar sebagai peserta sehingga Anda <b>tidak dapat memasuki tempat acara dan tidak berhak atas segala fasilitas acara.</b></p>
    <p style="color: #000000;">
        Punya pertanyaan? Hubungi kami di
        <a
        href="mailto:humas@gnome.id?subject=Daftar Peserta GNOME.Asia Summit 2019"
        >humas@gnome.id</a>
    </p>
</body>
</html>
