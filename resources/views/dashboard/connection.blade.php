@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1>Cek Connection To MyCOOP</h1>
    </div>
    <br/>
    <div class="row justify-content-center">
        <table class="table table-bordered table-datatable">
            <thead>
                <tr>
                    <th>Status Connection</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @if($data === true)
                        <td>
                           <h3>Connected</h3>
                        </td>
                    @else
                        <td>
                           <h3>Not Connected</h3>
                        </td>
                    @endif
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('javascript')
    <script>
        $(".table-datatable").DataTable();
    </script>
@endsection
