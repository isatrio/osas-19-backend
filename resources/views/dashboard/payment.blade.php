@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1>Cek payment by transaction ID</h1>
    </div>
    <form class="form-inline" method="POST" action="/sakti/cek/payment">
        @csrf
        <div class="form-group mx-sm-3 mb-2">
            <label for="inputTransactionID" class="sr-only">Transaction ID</label>
            <input type="number" class="form-control" name="transaction_id" id="inputTransactionID" placeholder="Transaction ID">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Cek transaction</button>
    </form>
    <br/>
    <div class="row justify-content-center">
        <table class="table table-bordered table-datatable">
            <thead>
                <tr>
                    <th>Transaction Id</th>
                    <th>Customer Number</th>
                    <th>Reference Id</th>
                    <th>Status</th>
                    <th>Transaction Date Time</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    @if($data->data->status === 'paid')
                        <tr>
                            <td>{{ $data->data->transaction_id }}</td>
                            <td>{{ $data->data->customer_number }}</td>
                            <td>{{ $data->data->reference_id }}</td>
                            <td>{{ $data->data->status }}</td>
                            <td>{{ $data->data->transaction_datetime }}</td>
                        </tr>
                    @else
                        <tr>
                            <td>{{ $data->data->transaction_id }}</td>
                            <td></td>
                            <td></td>
                            <td>{{ $data->data->status }}</td>
                            <td></td>
                        </tr>
                    @endif
                @endif
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('javascript')
    <script>
        $(".table-datatable").DataTable();
    </script>
@endsection
