@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1>Cek history payment by date</h1>
    </div>
    <form class="form-inline" method="POST" action="/sakti/cek/history">
        @csrf
        <div class="form-group mx-sm-3 mb-2">
            <label for="start_date" class="sr-only">Start Date</label>
            <input class="form-control date" id="start_date" name="start_date" type="text" placeholder="Start Date">
        </div>
        <div class="form-group mx-sm-3 mb-2">
            <label for="end_date" class="sr-only">End Date</label>
            <input class="form-control date" id="end_date" name="end_date" type="text" placeholder="End Date">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Cek transaction</button>
    </form>
    <table class="table table-bordered table-datatable">
        <thead>
            <tr>
                <th>Transaction Id</th>
                <th>Transaction Time</th>
                <th>Reference Id</th>
                <th>Customer Hp</th>
                <th>Amount</th>
                <th>Status</th>
        </thead>
        <tbody>
            @if($data)
                @foreach($data->data as $value)
                    <tr>
                        <td>{{ $value->transaction_id }}</td>
                        <td>{{ $value->transaction_time }}</td>
                        <td>{{ $value->reference_id }}</td>
                        <td>{{ $value->hp }}</td>
                        <td>{{ $value->amount }}</td>
                        @if($value->paid === 0)
                            <td>unpaid</td>
                        @else
                            <td>paid</td>
                        @endif
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $('.date').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
        $(".table-datatable").DataTable();
    </script>
@endsection
