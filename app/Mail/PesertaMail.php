<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PesertaMail extends Mailable
{
    use Queueable, SerializesModels;

    public $dataEmail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dataEmail)
    {
        $this->dataEmail = $dataEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('tiket@opensuse.id','Tiket openSUSE.Asia Summit 2019')->bcc('belombayar@sobatambyar.xyz')->subject('Tiket openSUSE.Asia Summit 2019')->view('email.peserta')->with('dataEmail', $this->dataEmail);
    }
}
