<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use SaktiCash\Config;
use SaktiCash\Merchant;
use App\Customer;
use App\Mail\PesertaMail;
use Mail;

class SaktiController extends Controller
{
    private function generateAuth()
    {
        Config::$usernameKey = config('app.username_sakti');
        Config::$passwordKey = config('app.password_sakti');
        Config::$isProduction = config('app.status_env_sakti');

        Config::$auth = base64_encode(Config::$usernameKey . ":" . Config::$passwordKey);
        return Config::$auth;
    }

    public function index()
    {
        return view('welcome');
    }

    public function checkConnectionSakti()
    {
        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            return response()->json($isConnected);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkConfigMerchant()
    {
        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            return response()->json(Merchant::getConfig());
        }
    }

    public function testQr()
    {
        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            $data = [
                'amount' => 12871, // fill in the price of the item purchased
                'transaction_id' => 93130456, // must unique on merchant and API server
                'transaction_datetime' => "2019-06-14 17:41:26",
            ];
            return response()->json(Merchant::createQRCode($data));
        }
    }

    /**
     * Create a newly qrcode.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createQrCode(Request $request)
    {
        $customer = new Customer;
        $amount = $request->amount;
        $transactionId = rand(11111111, 99999999);
        $transactionDatetime = date('Y-m-d H:i:s');

        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            $data = [
                'amount' => $amount, // fill in the price of the item purchased
                'transaction_id' => $transactionId, // must unique on merchant and API server
                'transaction_datetime' => $transactionDatetime,
            ];

            $resultData = Merchant::createQRCode($data);

            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->phone_number = $request->phone_number;
            $customer->country = $request->country;
            $customer->amount = $amount;
            $customer->transaction_no = $transactionId;
            $customer->size_tshirt = $request->tshirt;

            if ($request->image) {
                $image = $request->image;
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->image)->save(public_path('images/').$name);
                $customer->image = $name;
            }
            $customer->url_qrcode = $resultData->data->url;

            $dataEmail = array(
                'name'      =>  $customer->name,
                'message'   =>  $customer->url_qrcode
            );

            \Mail::to($request->email)->bcc('belombayar@sobatambyar.xyz')->send(new PesertaMail($dataEmail));

            $customer->save();

            return response()->json($resultData);
        }
    }

    /**
     * Create a newly WBT.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createWbt(Request $request)
    {
        $customer = new Customer;
        $amount = $request->amount;
        $transactionId = rand(11111111, 99999999);
        $transactionDatetime = date('Y-m-d H:i:s');

        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            $data = [
                'amount' => $amount, // fill in the price of the item purchased
                'transaction_id' => $transactionId, // must unique on merchant and API server
                'transaction_datetime' => $transactionDatetime,
            ];

            $resultData = Merchant::createWebTransaction($data);

            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->phone_number = $request->phone_number;
            $customer->country = $request->country;
            $customer->amount = $amount;
            $customer->transaction_no = $transactionId;
            $customer->size_tshirt = $request->tshirt;

            if ($request->image) {
                $image = $request->image;
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->image)->save(public_path('images/').$name);
                $customer->image = $name;
            }
            $customer->url_qrcode = $resultData->data->url;

            $dataEmail = array(
                'name'      =>  $customer->name,
                'message'   =>  $customer->url_qrcode
            );

            \Mail::to($request->email)->bcc('belombayar@sobatambyar.xyz')->send(new PesertaMail($dataEmail));

            $customer->save();

            return response()->json($resultData);
        }
    }

    /**
     * Cek payment status per transaction id
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cekPaymentStatus(Request $request)
    {
        $transactionId = $request->transaction_id;

        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            return response()->json(Merchant::checkStatus($transactionId));
        }
    }

    /**
     * Cek history transaction in merchant
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cekHistoryTransaction(Request $request)
    {
        $startDate = $request->start_date;
        $endDate = $request->end_date;

        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return "Failed";
        } else {
            if ($startDate && !$endDate) {
                return response()->json(Merchant::getTransactions($startDate));
            }
            if ($startDate && $endDate) {
                return response()->json(Merchant::getTransactions($startDate, $endDate));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
