<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Customer;
use SaktiCash\Config;
use SaktiCash\Merchant;
use Mail;
use App\Mail\TestMail;

class PesertaController extends Controller
{
    private function generateAuth()
    {
        Config::$usernameKey = config('app.username_sakti');
        Config::$passwordKey = config('app.password_sakti');
        Config::$isProduction = config('app.status_env_sakti');

        Config::$auth = base64_encode(Config::$usernameKey . ":" . Config::$passwordKey);
        return Config::$auth;
    }

    public function sendEmail()
    {
        $data = array('name'=>"Humas Gnome");
        Mail::send('email.test', $data, function($message) {
            $message->to('humas@gnome.id', 'Humas Gnome')
                    ->cc('isatrio.is.me@gmail.com')
                    ->subject('Tiket GNOME Asia Summit 2019');
            $message->from('humas@gnome.id','Tiket GNOME.Asia Summit 2019');
        });

        if (Mail::failures()) {
            return response()->Fail('Sorry! Please try again latter');
        }else{
            return response()->json('Yes, You have sent email with smtp mail.gnome.id');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.index');
    }

    /**
     * Display all data from customers / peserta.
     *
     * @return \Illuminate\Http\Response
     */
    public function json()
    {
        return Datatables::of(Customer::all())->make(true);
    }

    /**
     * Display a listing of the resource.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function connection(Request $request)
    {
        return view('dashboard.connection',[
            "data" => $this->getConnectionSakti($request)
        ]);
    }

    /**
     * Display a listing of the resource.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function merchant(Request $request)
    {
        return view('dashboard.merchant',[
            "data" => $this->getConfigMerchant($request)
        ]);
    }

    /**
     * Display a listing of the status connection.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getConnectionSakti(Request $request)
    {
        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return [];
        } else {
            return json_decode(json_encode($isConnected));
        }
    }

    /**
     * Display a listing of the config merchant.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getConfigMerchant(Request $request)
    {
        $this->generateAuth();
        $isConnected = Merchant::isConnected();

        if (!$isConnected) {
            return [];
        } else {
            return json_decode(json_encode(Merchant::getConfig()));
        }
    }

    /**
     * Display a listing of the resource.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request)
    {
        return view('dashboard.payment',[
            "data" => $this->getPayment($request)
        ]);
    }

    /**
     * get merchant
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getPayment(Request $request)
    {
        if ($request->transaction_id) {
            $transactionId = $request->transaction_id;

            $this->generateAuth();
            $isConnected = Merchant::isConnected();

            if (!$isConnected) {
                return [];
            } else {
                return json_decode(json_encode(Merchant::checkStatus($transactionId)));
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function history(Request $request)
    {
        return view('dashboard.history',[
            "data" => $this->getHistory($request)
        ]);
    }

    /**
     * get history
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getHistory(Request $request)
    {
        if ($request->start_date || $request->end_date) {
            $startDate = $request->start_date;
            $endDate = $request->end_date;

            $this->generateAuth();
            $isConnected = Merchant::isConnected();

            if (!$isConnected) {
                return [];
            } else {
                if ($startDate && !$endDate) {
                    return json_decode(json_encode(Merchant::getTransactions($startDate)));
                }
                if ($startDate && $endDate) {
                    return json_decode(json_encode(Merchant::getTransactions($startDate, $endDate)));
                }
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
